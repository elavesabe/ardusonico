/*
 * 
 * 
 * Conectar los 4 LDRs a A0 A1 A2 A3 A4 con una resistencia de 10k pulldown a Gnd en C/u 
 * y el Sr04 trigPin a D6 y echoPin a D7
 *
 * servo1 a D4 servo2 a D5 servo3 a D8  servo4 a D9 servo5 a D10
 *
 * 
 * Al final del Codido hay un delay de 50 mill para poder ver mejor en
 * el serialMonitor los valores. (borrarlo si laguea el sampleo de la performance)
 * tmb si lagea se puede achicar el delay de cada sensor que por defecto esta en 5 milliseg
 * 
 * Los LDRs Y Sr04 están mapeados ahi se pueden regular los valores max y min chequeando con el SerialMonitor
 * 
 * 
 * 
 */




#define trigPin 6
#define echoPin 7
#define led 3


#include <Servo.h>


Servo servo1; //controlado por SR04
Servo servo2; // controlado por LDR1
Servo servo3; // controlado por LDR2
Servo servo4; // controlado por LDR3
Servo servo5; // controlado por LDR4



const int ldr1 = A0;
const int ldr2 = A1;
const int ldr3 = A2;
const int ldr4 = A3;
const int ldr5 = A4;


int ldr1Value = 0;
int mapedLdr1Value = 0;

int ldr2Value = 0;
int mapedLdr2Value = 0;

int ldr3Value = 0;
int mapedLdr3Value = 0;

int ldr4Value = 0;
int mapedLdr4Value = 0;

int ldr5Value = 0;
int mapedLdr5Value = 0;


int mapedSr04_1Value = 0;

//booleanas que sirven para marcar cuando termina el pulso de cada servo
// Variables will change:
bool sr04State = LOW;   
          
bool ldr1State= LOW;
bool ldr2State= LOW;
bool ldr3State= LOW;
bool ldr4State= LOW;


//Setea los intervalos trabajando con la funcion Millis()
// Generally, you should use "unsigned long" for variables that hold time
// The value will quickly become too large for an int to store
unsigned long previousMillisSr04_1 = 0;        
unsigned long previousMillisLdr1 = 0;
unsigned long previousMillisLdr2 = 0;
unsigned long previousMillisLdr3 = 0;
unsigned long previousMillisLdr4 = 0;


void setup() {
  Serial.begin (115200);
  pinMode(trigPin, OUTPUT);
  pinMode(echoPin, INPUT);
  pinMode(led, OUTPUT);
  pinMode(2, OUTPUT);
  servo1.attach(4);
  servo2.attach(5);
  servo3.attach(8);
  servo4.attach(9);
  servo5.attach(10);
}

void loop() {

  

  
  // ------------------------------------
  // ------------- SR04-1 ---------------
  // --------------SETUP----------------


  long duration, distance;
  digitalWrite(trigPin, LOW);  // Added this line
  delayMicroseconds(2); // Added this line
  digitalWrite(trigPin, HIGH);
  //  delayMicroseconds(1000); - Removed this line
  delayMicroseconds(10); // Added this line
  digitalWrite(trigPin, LOW);
  duration = pulseIn(echoPin, HIGH);
  distance = (duration / 2) / 29.1;

   
  if (distance >= 200 || distance <= 0) {
    Serial.println("Out of range");
  }
  else {
    Serial.print(distance);
    Serial.println(" cm");
    Serial.println(duration);

  }

  mapedSr04_1Value = map(distance, 0, 200, 200, 4000);
  
  int intervalSr04_1 = mapedSr04_1Value;
  delay(5);

  
  // ------------------------------------
  // ------------- SR04-1 ---------------
  // ------------- SERVO ----------------

if(distance>1){
  unsigned long currentMillisSr04_1 = millis();


  if (currentMillisSr04_1 - previousMillisSr04_1 >= intervalSr04_1) {
    // save the last time you blinked the LED
    previousMillisSr04_1 = currentMillisSr04_1;

    // if the LED is off turn it on and vice-versa:
    if (sr04State == LOW) {
      sr04State = HIGH;
      servo1.write(40);
    } else {
      sr04State = LOW;
      servo1.write(0);
    }
  }}




  // ------------------------------------
  // ------------- Ldr-1 ----------------
  // ------------- SETUP ----------------


  
  ldr1Value = analogRead(ldr1);
  // map it to the range of the analog out:
  mapedLdr1Value = map(ldr1Value, 0, 1024, 200, 4000);
  // change the analog out value:

 int intervalLdr1 = mapedLdr1Value;

  // print the results to the Serial Monitor:
  Serial.print("ldr1value = ");
  Serial.print(ldr1Value);
  Serial.print("\t intervalLdr1 = ");
  Serial.println(intervalLdr1);

  // wait 2 milliseconds before the next loop for the analog-to-digital
  // converter to settle after the last reading:
  delay(2);

  // ------------------------------------
  // ------------- Ldr-1 ----------------
  // ------------- SERVO ----------------

  unsigned long currentMillisLdr1 = millis();

  if(ldr1Value<850){

  if (currentMillisLdr1 - previousMillisLdr1 >= intervalLdr1) {
    // save the last time you blinked the LED
    previousMillisLdr1 = currentMillisLdr1;

    if (ldr1State == LOW) {
      ldr1State= HIGH;
      servo2.write(40);
    } else {
      ldr1State = LOW;
      servo2.write(0);
    }
  }}


// ------------------------------------
  // ------------- Ldr-2 ----------------
  // ------------- SETUP ----------------


  
  ldr2Value = analogRead(ldr2);
  // map it to the range of the analog out:
  mapedLdr2Value = map(ldr2Value, 0, 1024, 200, 4000);
  // change the analog out value:

 int intervalLdr2 = mapedLdr2Value;

  // print the results to the Serial Monitor:
  Serial.print("ldr2value = ");
  Serial.print(ldr2Value);
  Serial.print("\t intervalLdr2 = ");
  Serial.println(intervalLdr2);

  // wait 2 milliseconds before the next loop for the analog-to-digital
  // converter to settle after the last reading:
  delay(2);

  // ------------------------------------
  // ------------- Ldr-2 ----------------
  // ------------- SERVO ----------------

  unsigned long currentMillisLdr2 = millis();

  if(ldr2Value<850){

  if (currentMillisLdr2 - previousMillisLdr2 >= intervalLdr2) {
    // save the last time you blinked the LED
    previousMillisLdr2 = currentMillisLdr2;

    if (ldr2State == LOW) {
      ldr2State= HIGH;
      servo3.write(40);
    } else {
      ldr2State = LOW;
      servo3.write(0);
    }
  }}

    // ------------------------------------
  // ------------- Ldr-3 ----------------
  // ------------- SETUP ----------------


  
  ldr3Value = analogRead(ldr3);
  // map it to the range of the analog out:
  mapedLdr3Value = map(ldr3Value, 0, 1024, 200, 4000);
  // change the analog out value:

 int intervalLdr3 = mapedLdr3Value;

  // print the results to the Serial Monitor:
  Serial.print("ldr3value = ");
  Serial.print(ldr3Value);
  Serial.print("\t intervalLdr3 = ");
  Serial.println(intervalLdr3);

  // wait 2 milliseconds before the next loop for the analog-to-digital
  // converter to settle after the last reading:
  delay(2);

  // ------------------------------------
  // ------------- Ldr-3 ----------------
  // ------------- SERVO ----------------

  unsigned long currentMillisLdr3 = millis();

  if(ldr3Value<850){

  if (currentMillisLdr3 - previousMillisLdr3 >= intervalLdr3) {
    // save the last time you blinked the LED
    previousMillisLdr3 = currentMillisLdr3;

    if (ldr3State == LOW) {
      ldr3State= HIGH;
      servo4.write(40);
    } else {
      ldr3State = LOW;
      servo4.write(0);
    }
  }}


// ------------------------------------
  // ------------- Ldr-4 ----------------
  // ------------- SETUP ----------------


  
  ldr4Value = analogRead(ldr4);
  // map it to the range of the analog out:
  mapedLdr4Value = map(ldr4Value, 0, 1024, 200, 4000);
  // change the analog out value:

 int intervalLdr4 = mapedLdr4Value;

  // print the results to the Serial Monitor:
  Serial.print("ldr4value = ");
  Serial.print(ldr4Value);
  Serial.print("\t intervalLdr4 = ");
  Serial.println(intervalLdr4);

  // wait 2 milliseconds before the next loop for the analog-to-digital
  // converter to settle after the last reading:
  delay(2);

  // ------------------------------------
  // ------------- Ldr-4 ----------------
  // ------------- SERVO ----------------

  unsigned long currentMillisLdr4 = millis();

  if(ldr4Value<850){

  if (currentMillisLdr4 - previousMillisLdr4 >= intervalLdr4) {
    // save the last time you blinked the LED
    previousMillisLdr4 = currentMillisLdr4;

    if (ldr4State == LOW) {
      ldr4State= HIGH;
      servo5.write(40);
    } else {
      ldr4State = LOW;
      servo5.write(0);
    }
  }}
  


}
