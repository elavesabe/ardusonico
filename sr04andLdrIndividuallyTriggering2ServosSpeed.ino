
/*
 * 
 * Conectar el LDR a A0 con una resistencia de 10k pull down a Gnd 
 * 
 * Al final del Codido hay un delay de 50 mill para poder ver mejor en
 * el serialMonitor los valores. 
 * 
 * El LDR está mapeado ahi se pueden regular los valores
 * chequeando con el SerialMonitor, mapear el Ultrasonico es una boludez tmb 
 * hay que hacerlo despues
 * 
 * 
 */

#define trigPin 6
#define echoPin 7
#define led 3


#include <Servo.h>


Servo servo1;
Servo servo2;
Servo servo3;
Servo servo4;
Servo servo5;

const int ledPin =  3;// the number of the LED pin
const int ldr1 = A0;
const int analogOutPin = 9;

int ldr1Value = 0;
int mapedLdr1Value = 0;

// Variables will change:
int ledState = LOW;             // ledState used to set the LED
int ldr1State= LOW;
// Generally, you should use "unsigned long" for variables that hold time
// The value will quickly become too large for an int to store
unsigned long previousMillisSr04_1 = 0;        // will store last time LED was updated
unsigned long previousMillisLdr1 = 0;
// constants won't change:
int pulso;

void setup() {
  Serial.begin (115200);
  pinMode(trigPin, OUTPUT);
  pinMode(echoPin, INPUT);
  pinMode(led, OUTPUT);
  pinMode(2, OUTPUT);
  servo1.attach(4);
  servo2.attach(5);
  servo3.attach(9);
  servo4.attach(10);
  servo5.attach(11);
  digitalWrite(2, LOW);
}

void loop() {

  

  
  // ------------------------------------
  // ------------- SR04-1 ---------------
  // --------------SETUP----------------


  long duration, distance;
  long intervalSr04_1 = pulso * 30;
  digitalWrite(trigPin, LOW);  // Added this line
  delayMicroseconds(2); // Added this line
  digitalWrite(trigPin, HIGH);
  //  delayMicroseconds(1000); - Removed this line
  delayMicroseconds(10); // Added this line
  digitalWrite(trigPin, LOW);
  duration = pulseIn(echoPin, HIGH);
  distance = (duration / 2) / 29.1;

  if (distance >= 200 || distance <= 0) {
    Serial.println("Out of range");
  }
  else {
    Serial.print(distance);
    Serial.println(" cm");
    Serial.println(duration);

  }
  pulso = distance;
  delay(5);

  
  // ------------------------------------
  // ------------- SR04-1 ---------------
  // ------------- SERVO ----------------

if(distance>0){
  unsigned long currentMillisSr04_1 = millis();


  if (currentMillisSr04_1 - previousMillisSr04_1 >= intervalSr04_1) {
    // save the last time you blinked the LED
    previousMillisSr04_1 = currentMillisSr04_1;

    // if the LED is off turn it on and vice-versa:
    if (ledState == LOW) {
      ledState = HIGH;
      servo1.write(40);
    } else {
      ledState = LOW;
      servo1.write(0);
    }

    // set the LED with the ledState of the variable:
    digitalWrite(led, ledState);
  }}




  // ------------------------------------
  // ------------- Ldr-1 ----------------
  // ------------- SETUP ----------------


  
  ldr1Value = analogRead(ldr1);
  // map it to the range of the analog out:
  mapedLdr1Value = map(ldr1Value, 0, 1024, -100, 2048);
  // change the analog out value:

 int intervalLdr1 = mapedLdr1Value;

  // print the results to the Serial Monitor:
  Serial.print("ldr1value = ");
  Serial.print(ldr1Value);
  Serial.print("\t intervalLdr1 = ");
  Serial.println(intervalLdr1);

  // wait 2 milliseconds before the next loop for the analog-to-digital
  // converter to settle after the last reading:
  delay(50);

  // ------------------------------------
  // ------------- Ldr-1 ----------------
  // ------------- SERVO ----------------

  unsigned long currentMillisLdr1 = millis();

  if(ldr1Value<850){

  if (currentMillisLdr1 - previousMillisLdr1 >= intervalLdr1) {
    // save the last time you blinked the LED
    previousMillisLdr1 = currentMillisLdr1;

    if (ldr1State == LOW) {
      ldr1State= HIGH;
      servo2.write(40);
    } else {
      ldr1State = LOW;
      servo2.write(0);
    }
  }}


}
